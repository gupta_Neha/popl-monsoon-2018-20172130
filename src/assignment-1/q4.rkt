#lang racket

(define (curry func n)
                ( let modcurry ((depth n) (arg_list '()))
                   (cond
                     [(equal? depth 0) (apply func (reverse arg_list))]
                     (else (lambda (new_arg) (modcurry (- depth 1)(cons new_arg arg_list)))))))

 

(provide curry)