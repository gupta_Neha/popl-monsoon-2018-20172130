#lang racket


(define (createBST value left-subtree right-subtree)
    (list value left-subtree right-subtree))

(define insert    
  (lambda (BST value)
    (cond
      ((null? BST)           
       (createBST value '() '())) 
      ((< value (first BST))    
       (createBST (first BST) 
                   (insert (first (rest BST)) value)
                   (first (rest (rest BST)))))
      ((> value (first BST))   
       (createBST (first BST)
                   (first (rest BST))
                   (insert (first (rest (rest BST))) value)))
      (else BST))))


(define (build-list-tree arg1)
  (define BST null)
  (cond
    [(list? arg1)
     (for ([i arg1])
    (set! BST (insert BST i)))
  (display BST)]
    (else "invalid")
   
   ) 
  
 )
    
(provide build-list-tree)